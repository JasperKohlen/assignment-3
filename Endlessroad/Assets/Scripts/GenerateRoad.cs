﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateRoad : MonoBehaviour
{
    public GameObject planePrefab;
    public Transform planePivot;
    private float spawnZ = 0.0f;
    private float tileLength = 10.0f;
    // Start is called before the first frame update
    
    void Start()
    {
        StartCoroutine(CreateRoad());   
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void spawnPlane()
    {
        GameObject go;
        go = Instantiate(planePrefab) as GameObject;
        go.transform.SetParent(transform);
        go.transform.position = Vector3.forward * spawnZ;
        spawnZ += tileLength;
    }

    IEnumerator CreateRoad()
    {
        while (true)
        {
            yield return new WaitForSeconds(1.0f);
            spawnPlane(); 
        }
    }
}
